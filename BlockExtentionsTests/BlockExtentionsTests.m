#import "BlockExtentionsTests.h"

#import "BlockExtentions.h"

@interface BlockExtentionsTests ()

@property (strong, nonatomic) NSArray* testData;
@property (strong, nonatomic) NSArray* testBigData;

@end

@implementation BlockExtentionsTests

@synthesize testData = _testData;

- (void)setUp
{
    [super setUp];
    self.testData = [NSArray arrayWithObjects:@"a", @"ab", @"b", @"bc", @"c", nil];
    self.testBigData = [NSArray arraySize:100500 generator:^id(NSUInteger index){ return @(arc4random()%42); }];
}

- (void)tearDown
{
    self.testData = nil;
    [super tearDown];
}

-(void)testNSArrayFindFirst
{
    STAssertEqualObjects( @"ab", [self.self.testData findFirst:_P( [_ rangeOfString:@"b"].length != 0 )], nil);
    STAssertEqualObjects( nil, [self.self.testData findFirst:_P( [_ rangeOfString:@"EEE"].length != 0 )], nil);
}

-(void)testNSArraySelect
{
    NSArray* selectArr = [self.testData select:_P( [_ rangeOfString:@"b"].length != 0 )];
    NSArray* resultArray = [NSArray arrayWithObjects:@"ab", @"b", @"bc", nil];

    STAssertEqualObjects( selectArr, resultArray, nil );
}

-(void)testNSArrayMap
{
    NSArray* map = [self.testData map:^(id str){ return [str uppercaseString]; }];
    NSArray* outArr = [NSArray arrayWithObjects:@"A", @"AB", @"B", @"BC", @"C", nil];

    STAssertEqualObjects( map, outArr, nil );
}

-(void)testMacro
{
    NSArray* lambdaArr = [self.testData map:lambda( var, [var uppercaseString] )];
    NSArray* shortMacroArr = [self.testData map:_( [_ uppercaseString] )];

    NSArray* outArr = [NSArray arrayWithObjects:@"A", @"AB", @"B", @"BC", @"C", nil];

    STAssertEqualObjects( outArr, lambdaArr, nil );
    STAssertEqualObjects( outArr, shortMacroArr, nil );
}

-(void)testLiteralsIndexing
{
    NSMutableArray* a = [@[ [NSNull null], @NO ] mutableCopy];
    a[0] = @NO;
    STAssertEquals(a[0], @NO, nil);
}

-(void)testAny
{
    NSArray* negative = @[@0, @1, @2, @3, @(-4), @5];
    STAssertTrue([negative any:^BOOL(id obj) { return [obj doubleValue] < 0; }], nil);
}

-(void)testAnyFail
{
    NSArray* negative = @[@0, @1, @2, @3, @4, @5];
    STAssertFalse([negative any:^BOOL(id obj) { return [obj doubleValue] < 0; }], nil);
}

-(void)testAll
{
    NSArray* positive = @[ @1, @2, @3, @4, @5 ];
    STAssertTrue([positive all:^BOOL(NSNumber* number) { return [number doubleValue] > 0; }], nil);
}

-(void)testAllFail
{
    NSArray* positive = @[ @1, @2, @(-3), @4, @5 ];
    STAssertFalse([positive all:^BOOL(NSNumber* number) { return [number doubleValue] > 0; }], nil);
}

-(void)testSum
{
    NSUInteger n = 100500;
    NSArray* array = [NSArray arraySize:n generator:^id(NSUInteger index) { return @(1); }];
    STAssertTrue( [[array sum:^NSNumber *(id obj) { return obj; }] integerValue] == n , nil);
}

@end
