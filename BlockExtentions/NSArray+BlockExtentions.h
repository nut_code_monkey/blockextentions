#import <Foundation/Foundation.h>

typedef id(^BEMapBlock)(id object);
typedef BOOL(^BEPredicateBlock)(id object);
typedef id(^BEReduceBlock)(id agregation, id object);
typedef id(^BEGeneratorBlock)(NSUInteger index);
typedef NSNumber*(^BEToNumberBlock)(id object);

@interface NSArray (BlockExtentions)

-(instancetype)map:( BEMapBlock )mapBlock;

-(instancetype)select:( BEPredicateBlock )predicate;

-(instancetype)reject:( BEPredicateBlock )predicate;

-(instancetype)rejectObject:(id)rejectedObject;

-(id)findFirst:( BEPredicateBlock )predicate;

-(BOOL)all:( BEPredicateBlock )predicate;

-(BOOL)any:( BEPredicateBlock )predicate;

-(id)reduce:( BEReduceBlock )reduceBlock initialValue:( id )initialValue;

-(NSSet*)set;

+(instancetype)arraySize:( NSUInteger )size generator:( BEGeneratorBlock )generatorBlock;

-(instancetype)classOf:( Class )classOf; // used: -isKindOfClass:

-(instancetype)sort; // used -compare:

-(instancetype)distinct;

-(instancetype)notNull;  // reject NSNull instances

-(NSNumber*)sum:( BEToNumberBlock )toNumber;

-(NSNumber*)avg:( BEToNumberBlock )toNumber;

@end


