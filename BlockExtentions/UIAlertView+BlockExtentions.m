//
//  UIAlertView+BlockExtentions.m
//  BlockExtentions
//
//  Created by Max on 10.11.13.
//
//

#import "UIAlertView+BlockExtentions.h"
#import <objc/runtime.h>

static char delegateKey;

typedef void(^BEAlertViewWithIndexCallback)(UIAlertView* alertView, NSInteger buttonIndex);
typedef void(^BEAlertViewCallback)(UIAlertView* alertView);

@interface BEAlertViewBlockExtentions : NSObject <UIAlertViewDelegate>

@property (copy, nonatomic) BEAlertViewWithIndexCallback alertViewClickedButtonAtIndex;
@property (copy, nonatomic) BEAlertViewCallback alertViewCancel;
@property (copy, nonatomic) BEAlertViewCallback willPresentAlertView;
@property (copy, nonatomic) BEAlertViewCallback didPresentAlertView;
@property (copy, nonatomic) BEAlertViewWithIndexCallback alertViewWillDismissWithButtonIndex;
@property (copy, nonatomic) BEAlertViewWithIndexCallback alertViewDidDismissWithButtonIndex;

@end

@implementation BEAlertViewBlockExtentions

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.alertViewClickedButtonAtIndex)
        self.alertViewClickedButtonAtIndex(alertView, buttonIndex);
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView
{
    if (self.alertViewCancel)
        self.alertViewCancel(alertView);
}

- (void)willPresentAlertView:(UIAlertView *)alertView  // before animation and showing view
{
    if (self.willPresentAlertView)
        self.willPresentAlertView(alertView);
}

- (void)didPresentAlertView:(UIAlertView *)alertView  // after animation
{
    if (self.didPresentAlertView)
        self.didPresentAlertView(alertView);
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex // before animation and hiding view
{
    if (self.alertViewWillDismissWithButtonIndex)
        self.alertViewWillDismissWithButtonIndex(alertView, buttonIndex);
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation
{
    if (self.alertViewDidDismissWithButtonIndex)
        self.alertViewDidDismissWithButtonIndex(alertView, buttonIndex);
}

@end


@implementation UIAlertView (BlockExtentions)

-(instancetype)alertViewStyle:( UIAlertViewStyle )style
{
    self.alertViewStyle = style;
    return self;
}

-(instancetype)alertViewClickedButtonAtIndex:( BEAlertViewWithIndexCallback )callback
{
    
    self.alertViewDelegate.alertViewClickedButtonAtIndex = callback;
    return self;
}

-(instancetype)alertViewCancel:( BEAlertViewCallback )callback
{
    self.alertViewDelegate.alertViewCancel = callback;
    return self;
}

-(instancetype)willPresentAlertView:( BEAlertViewCallback )callback
{
    self.alertViewDelegate.willPresentAlertView = callback;
    return self;
}

-(instancetype)didPresentAlertView:( BEAlertViewCallback )callback
{
    self.alertViewDelegate.didPresentAlertView = callback;
    return self;
}

-(BEAlertViewBlockExtentions*)alertViewDelegate
{
    return objc_getAssociatedObject(self, &delegateKey);
}

-(void)setAlertViewDelegate:( BEAlertViewBlockExtentions* )delegate
{
    objc_setAssociatedObject(self, &delegateKey, delegate, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+(instancetype)alertViewWithTitle:(NSString *)title
                          message:(NSString *)message
                cancelButtonTitle:(NSString *)cancelButtonTitle
                otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    BEAlertViewBlockExtentions* delegate = [BEAlertViewBlockExtentions new];
    
    UIAlertView* alertView = [[self alloc] initWithTitle:title
                                                 message:message
                                                delegate:delegate
                                       cancelButtonTitle:cancelButtonTitle
                                       otherButtonTitles:nil];
    if (alertView)
    {
        alertView.alertViewDelegate = delegate;
        
		id eachObject;
		va_list argumentList;
		if (otherButtonTitles)
        {
			[alertView addButtonWithTitle:otherButtonTitles];
			va_start(argumentList, otherButtonTitles);
			while ((eachObject = va_arg(argumentList, id)))
            {
				[alertView addButtonWithTitle:eachObject];
			}
			va_end(argumentList);
		}
    }
    return alertView;
}

@end
