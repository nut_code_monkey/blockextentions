//
//  UIAlertView+BlockExtentions.h
//  BlockExtentions
//
//  Created by Max on 10.11.13.
//
//

#import <UIKit/UIKit.h>

typedef void(^BEAlertViewWithIndexCallback)(UIAlertView* alertView, NSInteger buttonIndex);
typedef void(^BEAlertViewCallback)(UIAlertView* alertView);

@interface UIAlertView (BlockExtentions)

+(instancetype)alertViewWithTitle:(NSString *)title
                          message:(NSString *)message
                cancelButtonTitle:(NSString *)cancelButtonTitle
                otherButtonTitles:(NSString *)otherButtonTitles, ...;

-(instancetype)alertViewStyle:( UIAlertViewStyle )style;

-(instancetype)alertViewClickedButtonAtIndex:( BEAlertViewWithIndexCallback )callback;
-(instancetype)alertViewCancel:( BEAlertViewCallback )callback;
-(instancetype)willPresentAlertView:( BEAlertViewCallback )callback;
-(instancetype)didPresentAlertView:( BEAlertViewCallback )callback;

@end
