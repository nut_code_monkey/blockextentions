#import "NSNull+BlockExtentions.h"

typedef void(^IfNullBlock)();


@implementation NSObject (BlockExtentions)

-(void)null:( IfNullBlock )ifNullBlock notNull:( IfNullBlock )elseBlock
{
    if (!elseBlock)
        elseBlock = ^{};

    elseBlock();
}

@end

@implementation NSNull (BlockExtentions)

-(void)null:( IfNullBlock )ifNullBlock notNull:( IfNullBlock )elseBlock
{
    if (!ifNullBlock)
        ifNullBlock = ^{};

    ifNullBlock();
}

@end
