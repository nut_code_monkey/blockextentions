#import <Foundation/Foundation.h>

@interface NSObject (BlockExtentions)

-(void)null:( void(^)() )ifNullBlock notNull:( void(^)() )elseBlock;

@end


@interface NSNull (BlockExtentions)
@end
