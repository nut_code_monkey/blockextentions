#import "NSArray+BlockExtentions.h"

@implementation NSArray (BlockExtentions)

-(instancetype)map:( BEMapBlock )mapBlock
{
    NSParameterAssert( mapBlock );
    NSMutableArray* outArray = [self mutableCopy];
    [self enumerateObjectsUsingBlock:^(id object, NSUInteger index, BOOL* stop)
    {
        outArray[index] = mapBlock(object) ?: [NSNull null];
    }];
    return [NSArray arrayWithArray:outArray];
}

-(instancetype)select:( BEPredicateBlock )predicate
{
    NSParameterAssert( predicate );
    NSMutableArray* outArray = [NSMutableArray array];
    for (id object in self)
    {
        if ( predicate(object) )
        {
            [outArray addObject:object];
        }
    }
    return [NSArray arrayWithArray:outArray];
}

-(instancetype)reject:( BEPredicateBlock )predicate
{
    NSParameterAssert( predicate );
    NSMutableArray* outArray = [NSMutableArray array];
    for (id object in self)
    {
        if ( !predicate(object) )
        {
            [outArray addObject:object];
        }
    }
    return [NSArray arrayWithArray:outArray];
}

-(instancetype)rejectObject:(id)rejectedObject
{
    NSParameterAssert(rejectedObject);

    NSMutableArray* array = [self mutableCopy];
    [array removeObject:rejectedObject];
    return [NSArray arrayWithArray:array];
}

-(id)findFirst:( BEPredicateBlock )findPredicate
{
    NSParameterAssert( findPredicate );
    for (id object in self)
    {
        if ( findPredicate(object) )
        {
            return object;
        }
    }
    return nil;
}

-(instancetype)classOf:( Class )classOf
{
    NSParameterAssert( classOf );
    return [self select:^BOOL(id object){ return [object isKindOfClass:classOf]; }];
}

-(instancetype)sort
{
    return [self sortedArrayWithOptions:NSSortConcurrent
                        usingComparator:^(id obj1, id obj2){ return [obj1 compare:obj2]; }];
}

-(instancetype)distinct
{
    NSSet* distinctSet = [self set];
    return [self select:^BOOL(id object) { return [distinctSet containsObject:object]; }];
}

-(instancetype)notNull
{
    return [self reject:^BOOL(id object) { return [object isKindOfClass:[NSNull class]]; }];
}

-(BOOL)all:( BEPredicateBlock )predicate
{
    NSParameterAssert( predicate );
    __block NSUInteger countOfObjectsPassedTest = 0;
    [self enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop)
     {
         if ( predicate(object) )
         {
             ++countOfObjectsPassedTest;
         }
         else
         {
             *stop = YES;
         }
     }];

    return countOfObjectsPassedTest == self.count;
}

-(BOOL)any:( BEPredicateBlock )predicate
{
    NSParameterAssert( predicate );
    return [self findFirst:predicate] != nil;
}

-(instancetype)reduce:( BEReduceBlock )reduceBlock initialValue:( id )initialValue
{
    NSParameterAssert(reduceBlock);
    id agregation = initialValue;
    for (id object in self)
    {
        agregation = reduceBlock(agregation, object);
    }
    return agregation;
}

-(NSSet*)set
{
    return [NSSet setWithArray:self];
}

+(instancetype)arraySize:( NSUInteger )size generator:( BEGeneratorBlock )generatorBlock
{
    return [self arrayWithArray:[NSMutableArray arraySize:size generator:generatorBlock]];
}

-(NSNumber*)sum:( BEToNumberBlock )toNumber
{
    NSParameterAssert(toNumber);
    return [self reduce:^id(NSDecimalNumber* sum, id object)
            {
                return [sum decimalNumberByAdding:[NSDecimalNumber decimalNumberWithDecimal:[toNumber(object) decimalValue]]];
            }
           initialValue:[NSDecimalNumber zero]];
}

-(NSNumber *)avg:( BEToNumberBlock )toNumber
{
    NSParameterAssert(toNumber);
    return [[NSDecimalNumber decimalNumberWithDecimal:[[self sum:toNumber] decimalValue]]
            decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithDecimal:[@(self.count) decimalValue]]];
}

@end

@implementation NSMutableArray (BlockExtentions)

+(instancetype)arraySize:( NSUInteger )size generator:( id(^)(NSUInteger) )generatorBlock
{
    NSParameterAssert( generatorBlock );
    NSMutableArray* array = [self arrayWithCapacity:size];
    for (NSUInteger i = 0; i < size; ++i)
    {
        [array addObject:generatorBlock(i) ?: [NSNull null]];
    }
    return array;
}

@end
