//
//  NSSet+BlockExtentions.h
//  BlockExtentions
//
//  Created by Max on 12.08.13.
//

#import <Foundation/Foundation.h>

@interface NSSet (BlockExtentions)

-(instancetype)setMinusSet:( NSSet* )set;
-(instancetype)setUnionSet:( NSSet* )set;
-(instancetype)setIntersectSet:( NSSet* )set;

@end
