//
//  NSSet+BlockExtentions.m
//  BlockExtentions
//
//  Created by Max on 12.08.13.
//

#import "NSSet+BlockExtentions.h"

@implementation NSSet (BlockExtentions)

-(instancetype)setMinusSet:( NSSet* )set
{
    NSMutableSet* mutableSet = [self mutableCopy];
    [mutableSet minusSet:set];
    return [NSSet setWithSet:mutableSet];
}

-(instancetype)setUnionSet:( NSSet* )set
{
    NSMutableSet* mutableSet = [self mutableCopy];
    [mutableSet unionSet:set];
    return [NSSet setWithSet:mutableSet];
}

-(instancetype)setIntersectSet:( NSSet* )set
{
    NSMutableSet* mutableSet = [self mutableCopy];
    [mutableSet intersectSet:set];
    return [NSSet setWithSet:mutableSet];
}

@end
