Simple block-based extentions.
==============================

Methods added to NSArray:
-------------------------

-(instancetype)map:( id(^)(id object) )mapBlock;

-(instancetype)select:( BOOL(^)(id object) )predicate;

-(instancetype)reject:( BOOL(^)(id object) )predicate;

-(instancetype)rejectObject:(id)rejectedObject;

-(id)findFirst:( BOOL(^)(id object) )predicate;

-(BOOL)all:( BOOL(^)(id object) )predicate;

-(BOOL)any:( BOOL(^)(id object) )predicate;

-(id)reduce:( id(^)(id agregation, id object) )reduceBlock initialValue:( id )initialValue;

-(NSSet*)set;

+(instancetype)arraySize:( NSUInteger )size generator:( id(^)(NSUInteger index) )generatorBlock;

-(instancetype)classOf:( Class )classOf; // used: -isKindOfClass:

-(instancetype)sort; // used -compare:

-(instancetype)distinct;

-(instancetype)notNull;  // reject NSNull instances

-(NSNumber*)sum:( NSNumber*(^)(id object) )toNumber;

-(NSNumber*)avg:( NSNumber*(^)(id object) )toNumber;

Methods added to NSSet:
-----------------------

-(instancetype)setMinusSet:( NSSet* )set;

-(instancetype)setUnionSet:( NSSet* )set;

-(instancetype)setIntersectSet:( NSSet* )set;

Methods added to UIAlertView:
-----------------------------

+(instancetype)alertViewWithTitle:(NSString *)title
                          message:(NSString *)message
                cancelButtonTitle:(NSString *)cancelButtonTitle
                otherButtonTitles:(NSString *)otherButtonTitles, ...;

-(instancetype)alertViewStyle:( UIAlertViewStyle )style;

-(instancetype)alertViewClickedButtonAtIndex:( void(^)(UIAlertView* alert, NSUIntger idx) )callback;

-(instancetype)alertViewCancel:( void(^)(UIAlertView* alert) )callback;

-(instancetype)willPresentAlertView:( void(^)(UIAlertView* alert) )callback;

-(instancetype)didPresentAlertView:( void(^)(UIAlertView* alert) )callback;
